
import Table, { AvatarCell, SelectColumnFilter, StatusPill } from './Table'  // new
import axios from 'axios';
import React, { useState ,useEffect} from "react";

const CURRENCY_API_URL = `http://d7fb-2a02-c207-2070-7324-00-1.ngrok.io/currencies/breakoutsDetails`;

export const retrieveCurenciesDetailsState = () => {
  console.log("Calling Curencies API " + CURRENCY_API_URL);

 return new Promise((resolve, reject) => {

      axios.get(CURRENCY_API_URL).then(res => {
          resolve(res.data);
       //   console.log(res.data);
      }).catch(err => reject(err));
  });
};

const getData = () => {
  const data = [
    {
      name: 'Jane Cooper',
      email: 'jane.cooper@example.com',
      title: 'Regional Paradigm Technician',
      department: 'Optimization',
      status: 'Active',
      role: 'Admin',
      age: 27,
      imgUrl: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
    },
    {
      name: 'Cody Fisher',
      email: 'cody.fisher@example.com',
      title: 'Product Directives Officer',
      department: 'Intranet',
      status: 'Inactive',
      role: 'Owner',
      age: 43,
      imgUrl: 'https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
    },
    {
      name: 'Esther Howard',
      email: 'esther.howard@example.com',
      title: 'Forward Response Developer',
      department: 'Directives',
      status: 'Active',
      role: 'Member',
      age: 32,
      imgUrl: 'https://images.unsplash.com/photo-1520813792240-56fc4a3765a7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
    },
    {
      name: 'Jenny Wilson',
      email: 'jenny.wilson@example.com',
      title: 'Central Security Manager',
      department: 'Program',
      status: 'Offline',
      role: 'Member',
      age: 29,
      imgUrl: 'https://images.unsplash.com/photo-1498551172505-8ee7ad69f235?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
    },
    {
      name: 'Kristin Watson',
      email: 'kristin.watson@example.com',
      title: 'Lean Implementation Liaison',
      department: 'Mobility',
      status: 'Inactive',
      role: 'Admin',
      age: 36,
      imgUrl: 'https://images.unsplash.com/photo-1532417344469-368f9ae6d187?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
    },
    {
      name: 'Cameron Williamson',
      email: 'cameron.williamson@example.com',
      title: 'Internal Applications Engineer',
      department: 'Security',
      status: 'Active',
      role: 'Member',
      age: 24,
      imgUrl: 'https://images.unsplash.com/photo-1566492031773-4f4e44671857?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
    },
  ]
  return [...data, ...data, ...data]
}

function App() {

  const columns = React.useMemo(() => [
    {
      Header: "Id",
      accessor: 'id',
      Cell: AvatarCell,
      imgAccessor: "imgUrl",
      emailAccessor: "email",
    },
    {
      Header: "session",
      accessor: 'session',
    },
    {
      Header: "pairCurrency",
      accessor: 'pairCurrency',
      Cell: StatusPill,
    },
    {
      Header: "created",
      accessor: 'created',
    },
    {
      Header: "timeFrame",
      accessor: 'timeFrame',
    },
    {
      Header: "timeZone",
      accessor: 'timeZone',
    },
    {
      Header: "lowPrice",
      accessor: 'lowPrice',
    },
    {
      Header: "highPrice",
      accessor: 'highPrice',
    },
    {
      Header: "strength",
      accessor: 'strength',
    },
    {
      Header: "direction",
      accessor: 'direction',

    },
    {
      Header: "currenciesStrengthDetails",
      accessor: 'currenciesStrengthDetails',
    },
    {
      Header: "changeRate",
      accessor: 'changeRate',
    },
    {
      Header: "rowColor",
      accessor: 'rowColor',
    },

    {
      Header: "pivotPoint",
      accessor: 'role',
      Filter: SelectColumnFilter,  // new
      filter: 'includes',
    },
  ], [])

  var data2 = []

  axios.get(CURRENCY_API_URL).then((response) => {
     data2 = response.data;
     console.log(response.data)
  });
  
  console.log(data2)
  const data = React.useMemo(() => data2, [])
  console.log(data)

  return (
    <div className="min-h-screen bg-gray-100 text-gray-900">
      <main className="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8 pt-4">
        <div className="">
          <h1 className="text-xl font-semibold">React Table + Tailwind CSS = ❤</h1>
        </div>
        <div className="mt-6">
          <Table columns={columns} data={data} />
        </div>
      </main>
    </div>
  );
}

export default App;
