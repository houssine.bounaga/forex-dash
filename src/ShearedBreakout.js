import Table, { AvatarCell, SelectColumnFilter, StatusPill } from './Table'  // new
import axios from 'axios';
import React, { useState, useEffect, Component } from "react";

const CURRENCY_SHEARED_API_URL = `http://d7fb-2a02-c207-2070-7324-00-1.ngrok.io/breakouts/findBreakoutSharedPair`;

export const headers = [

    {
        Header: "Currency",
        accessor: 'currency',     
    },
    {
        Header: "time",
        accessor: 'time',
        maxWidth: 10,
    },
    {
        Header: "count",
        accessor: 'count',
        maxWidth: 10,
    },
    {
        Header: "shared Currencies",
        accessor: 'sharedCurrencies',

    },
    {
        Header: "currencies Strength",
        accessor: 'currenciesStrength',

    }
]

export default class ShearedBreakout extends Component {
    
    constructor(props) {
        super(props);
    
        this.state = {
         columns: headers,
         shearedPair : [],
        };

        axios.get(CURRENCY_SHEARED_API_URL).then((response) => {
            this.setState ({ shearedPair : response.data}) 
         });
      }
   
   
    render() {
        return (
            <div className="min-h-screen bg-gray-100 text-gray-900">
                <main className="max-w-fit mx-auto px-4 sm:px-6 lg:px-8 pt-4" >
                    <div className="">
                        <h1 className="text-xl font-semibold">Shared Pair </h1>
                    </div>
                    <div className="mt-6">
                        <Table columns={this.state.columns} data={this.state.shearedPair} />
                    </div>
                
                </main>
            </div>
        );
    }
}

