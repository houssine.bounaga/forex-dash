
import Table, { AvatarCell, SelectColumnFilter, StatusPill } from './Table'  // new
import axios from 'axios';
import React, { useState, useEffect, Component } from "react";

const CURRENCY_CHANGE_1H_API_URL = `http://d7fb-2a02-c207-2070-7324-00-1.ngrok.io/forexScrapper/getCurrenciesStrengthByChange?timeFrame=PERIOD_H1`;
const CURRENCY_CHANGE_4H_API_URL = `http://d7fb-2a02-c207-2070-7324-00-1.ngrok.io/forexScrapper/getCurrenciesStrengthByChange?timeFrame=PERIOD_H4`;
const CURRENCY_CHANGE_1D_API_URL = `http://d7fb-2a02-c207-2070-7324-00-1.ngrok.io/forexScrapper/getCurrenciesStrengthByChange?timeFrame=PERIOD_D1`;

export const headers = [

    {
        Header: "Currency",
        accessor: 'label',     
    },
    {
        Header: "time",
        accessor: 'time',
        maxWidth: 10,
    },
    {
        Header: "percentChange",
        accessor: 'percentChange',

    },
    {
        Header: "trend",
        accessor: 'trend',
        maxWidth: 10,
    }
]

export default class ChangeCurrencies extends Component {
    
    constructor(props) {
        super(props);
    
        this.state = {
         columns: headers,
         change_1H : [],
         change_4H : [],
         change_1D : []
        };

        axios.get(CURRENCY_CHANGE_1H_API_URL).then((response) => {
            this.setState ({ change_1H : response.data}) 
         });
         axios.get(CURRENCY_CHANGE_4H_API_URL).then((response) => {
            this.setState ({ change_4H : response.data}) 
         });
         axios.get(CURRENCY_CHANGE_1D_API_URL).then((response) => {
            this.setState ({ change_1D : response.data}) 
         });
      }
   
   
    render() {
        return (
            <div className="min-h-screen bg-gray-100 text-gray-900">
                <main className="max-w-fit mx-auto px-4 sm:px-6 lg:px-8 pt-4 flex flex-row" >
                    <div className="">
                        <h1 className="text-xl font-semibold">Curriencies Change 1H</h1>
                    </div>
                    <div className="mt-6">
                        <Table columns={this.state.columns} data={this.state.change_1H} />
                    </div>
                    <div className="">
                        <h1 className="text-xl font-semibold">Curriencies Change 4H</h1>
                    </div>
                    <div className="mt-6" >
                        <Table columns={this.state.columns} data={this.state.change_4H} />
                    </div>
                    <div className="">
                        <h1 className="text-xl font-semibold">Curriencies Change 1D</h1>
                    </div>
                    <div className="mt-6" >
                        <Table columns={this.state.columns} data={this.state.change_1D} />
                    </div>
                </main>
            </div>
        );
    }
}

