import React, { useState, useEffect, Component } from "react";
import { Calendar } from 'react-date-range';
import { Button } from 'react-bootstrap';
import axios from 'axios';
import * as moment from 'moment'
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file

const SETTINGS_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/settings/resetSessionRangeByDate`;

export default class Settings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
        this.handleSelect = this.handleSelect.bind(this);
        this.sendChangeHandler = this.sendChangeHandler.bind(this);
    }
    
    handleSelect(selectedDate) {
        this.setState({
            date: selectedDate
        });
        console.log(selectedDate);
    }

    async sendChangeHandler(event) {
        event.preventDefault();
        const entity = { };
        let currentDate = moment(this.state.date).format("YYYY-MM-DDTHH:mm:ss");
        const url = SETTINGS_API_URL + "?date=" + currentDate;
        const response = await axios.post(url, entity);
    }
    
    render() {
        return (
            <div className="min-h-screen bg-gray-100 text-gray-900">
                <div className='text-center'>
                    <h2>Reset breakout Seesion</h2>
                </div>
                <form onSubmit={this.sendChangeHandler} className="change-header-form">
                    <Calendar
                        date={this.state.date}
                        onChange={this.handleSelect}
                    />
                    <div className="min-h-screen bg-gray-100 text-gray-900">
                        <Button as="input" type="submit" value="Submit" />{' '}
                    </div>
                </form>
            </div>
        )

    }
}