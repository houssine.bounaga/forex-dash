import React from 'react';
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from 'cdbreact';
import { NavLink } from 'react-router-dom';
import CurrienciesProvider from './CurrienciesProvide';
import ChangeCurrencies from './ChangeCurrencies';
import ShearedBreakout from './ShearedBreakout';
import Settings from './Settings';
import StrengthDashBoard from './StrengthDashBoard';



import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom';

const Sidebar = () => {
  return (
    <div style={{ display: 'flex', height: '100vh', overflow: 'scroll initial' }}>
      <CDBSidebar textColor="#fff" backgroundColor="#333">
        <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
          <a href="/" className="text-decoration-none" style={{ color: 'inherit' }}>
            FX Analytics
          </a>
        </CDBSidebarHeader>

        <CDBSidebarContent className="sidebar-content">
          <CDBSidebarMenu>
            <NavLink exact to="/strengthDetails" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="columns">Dashboard</CDBSidebarMenuItem>
            </NavLink>
            <NavLink exact to="/breakoutsDetails" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="table">Breakouts Details</CDBSidebarMenuItem>
            </NavLink>
  {/*           <NavLink exact to="/profile" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="user">Profile page</CDBSidebarMenuItem>
            </NavLink> */}
            <NavLink exact to="/changeMovement" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="chart-line">Change Movement</CDBSidebarMenuItem>
            </NavLink>

            <NavLink exact to="/shearedBreakout" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="sticky-note">Sheared Breakouts</CDBSidebarMenuItem>
            </NavLink>
            <NavLink exact to="/settings" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="user">Settings</CDBSidebarMenuItem>
            </NavLink>
          </CDBSidebarMenu>

        </CDBSidebarContent>

        <CDBSidebarFooter style={{ textAlign: 'center' }}>
          <div
            style={{
              padding: '20px 5px',
            }}
          >
            Sidebar Footer
          </div>
        </CDBSidebarFooter>
      </CDBSidebar>
      <Routes>
      
        <Route exact path='/strengthDetails' element={< StrengthDashBoard />}></Route>
        <Route exact path='/breakoutsDetails' element={< CurrienciesProvider />}></Route>
        <Route exact path='/changeMovement' element={< ChangeCurrencies />}></Route>
        <Route exact path='/shearedBreakout' element={< ShearedBreakout />}></Route> 
        <Route exact path='/settings' element={< Settings />}></Route> 
      </Routes>
    </div>
  
  );
};

export default Sidebar;