
import Table, { AvatarCell, SelectColumnFilter, StatusPill,NewsCell } from './Table'  // new
import axios from 'axios';
import React, { useState, useEffect, Component } from "react";
import * as moment from 'moment'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const CURRENCY_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/breakouts/breakoutsDetails`;
const NOTIFICATION_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/notification?`;
const BREAKOUT_CALCULATE_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/breakouts/calculatePairBreakout?sessions=ASIA,LONDON`;


export const headers = [
    // {
    //     Header: "Id",
    //     accessor: 'id',
    //     Cell: AvatarCell,
    //     imgAccessor: "imgUrl",
    //     emailAccessor: "email",
    // },
    {
        Header: "session",
        accessor: 'session',
        Filter: SelectColumnFilter,  // newc
    },
    {
        Header: "Currency",
        accessor: 'pairCurrency',
        Filter: SelectColumnFilter,  // new
        filter: 'includes',
      
    },
    {
        Header: "created",
        accessor: 'created',
    },
    {
        Header: "Status",
        accessor: 'breakoutStatus',
        Filter: SelectColumnFilter,  // new
        filter: 'includes',
        Cell: StatusPill, 
    },
    {
        Header: "direction",
        accessor: 'direction',
        Cell: AvatarCell,
    },
    {
        Header: "currencies Strength Details",
        accessor: 'currenciesStrengthDetails',
    },
    {
        Header: "news",
        accessor: 'newsImpact',
        Cell: NewsCell,
     
    },
    {
        Header: "low Price",
        accessor: 'lowPrice',
    },
    {
        Header: "high Price",
        accessor: 'highPrice',
    },

    {
        Header: "time Frame",
        accessor: 'timeFrame',
    },
    {
        Header: "strength",
        accessor: 'strength',
        
    },
/* 
    {
        Header: "change Rate",
        accessor: 'changeRate',
    },
    {
        Header: "iRsi",
        accessor: 'iRsi',
    },
    {
        Header: "stochasticRsi",
        accessor: 'stochasticRsi',
    },
   {
        Header: "pivot Point",
        accessor: 'pivotPoint',
      
    }, */
    {
        Header: "rowColor",
        accessor: 'rowColor',
      //  Filter: SelectColumnFilter,  // new
      //  filter: 'includes',
    },
    {
        Header: "ID",
        accessor: 'breakoutId',
    }
]

export default class CurrienciesProvider extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
         columns: headers,
         curriencies : [],
         notifications : []
        };
        this.refreshPage = this.refreshPage.bind(this);

        axios.get(CURRENCY_API_URL).then((response) => {
            this.setState ({ curriencies : response.data}) 
         
         });
         const limit = 2;
         let i = 1;
         const interval = setInterval(() => {
         
             // let yourdate = '2022-05-12T09:15:00'; 
             const time = new Date();
             const numOfHours = 2;
             time.setTime(time.getTime() + numOfHours * 60 * 60 * 1000);
             let date = moment(time).format("YYYY-MM-DDTHH:mm:ss");
             const url = NOTIFICATION_API_URL + "start=" + date;

            axios.get(url).then((response) => {
                this.setState({ notifications: response.data })
            });
             this.state.notifications.map((notification) => {
                // NotificationManager.success(notification.description, notification.title);
                 new Notification(notification.title + " " + notification.description);
             })
             i++;
             if (i > limit) {
                clearInterval(interval);
                console.log('interval cleared!');
              }

          }, 900_000); // 900_000 15 min
       // clearInterval(interval);
      }
       addHours(numOfHours, date = new Date()) {
        date.setTime(date.getTime() + numOfHours * 60 * 60 * 1000);
      
        return date;
      }

      componentDidMount() {
          
        
        if (!("Notification" in window)) {
          console.log("This browser does not support desktop notification");
        } else {
      
        }
    }

    refreshPage() {

        let startTime = new Date().setMinutes(0);
        let endTime=  new Date().setMinutes(0);
        let start = moment(startTime).format("YYYY-MM-DDT");
        let end = moment(endTime).format("YYYY-MM-DDT");
        
        const url = BREAKOUT_CALCULATE_API_URL + "&start=" + start +"09:00:00"+ "&end="+end+"20:00:00";
        axios.get(url).then((response) => {
            this.setState({ curriencies: response.data })
        });
        window.location.reload();
    }

    render() {
        return (

            <div className="min-h-screen bg-gray-100 text-gray-900">

                <main className="max-w-fit mx-auto px-4 sm:px-6 lg:px-8 pt-4">
                    <div className="">
                        <h1 className="text-xl font-semibold">Curriencies Breakouts Details</h1>
                </div>
                <div className="mt-6">
                   <Table columns={this.state.columns} data={this.state.curriencies} />
                </div>
                <link rel="stylesheet" type="text/css" href="path/to/notifications.css"></link>
              </main>
            </div>
            
          );
      }
}

