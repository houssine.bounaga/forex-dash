
import StrengthTable, { directionCell, SelectColumnFilter, StatusPill,NewsCell } from './StrengthTable'  // new
import axios from 'axios';
import React, { useState, useEffect, Component } from "react";
import * as moment from 'moment'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const CURRENCY_METER_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/technicals/currencyMeterDetails`;

const CURRENCY_METER_1H_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/technicals/currencyMeterDetails?period=PERIOD_H1`;
const CURRENCY_METER_4H_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/technicals/currencyMeterDetails?period=PERIOD_H4`;
const CURRENCY_METER_1D_API_URL = `https://207e-2a02-c207-2070-7324-00-1.ngrok.io/technicals/currencyMeterDetails?period=PERIOD_D1`;



export const headers = [

    {
        Header: "position",
        accessor: 'position',
    },
    {
        Header: "Currency",
        accessor: 'currency',
      
    },
    {
        Header: "value",
        accessor: 'value',
    },

    {
        Header: "direction",
        accessor: 'direction',
        Cell: directionCell,
    },
    {
        Header: "time",
        accessor: 'time',
    }
]

export default class StrengthDashBoard extends Component {
    
    constructor(props) {
        super(props);
    
        this.state = {
         columns: headers,
         change_1H : [],
         change_4H : [],
         change_1D : []
        };

        axios.get(CURRENCY_METER_1H_API_URL).then((response) => {
            this.setState ({ change_1H : response.data}) 
         });
         axios.get(CURRENCY_METER_4H_API_URL).then((response) => {
            this.setState ({ change_4H : response.data}) 
         });
         axios.get(CURRENCY_METER_1D_API_URL).then((response) => {
            this.setState ({ change_1D : response.data}) 
         });
      }
   
   
      render() {
        return (
            <div className="min-h-screen bg-gray-100 text-gray-900">
                <main className="max-w-fit mx-auto px-4 sm:px-6 lg:px-8 pt-4 flex flex-row" >
                
                    <div className="mt-6">
                    <h1 className="text-xl font-semibold">Currency Meter 1H</h1>
                        <StrengthTable columns={this.state.columns} data={this.state.change_1H} />
                    </div>
              
                    <div className="mt-6" >
                    <h1 className="text-xl font-semibold">Currency Meter 4H</h1>
                        <StrengthTable columns={this.state.columns} data={this.state.change_4H} />
                    </div>
                
                    <div className="mt-6" >
                    <h1 className="text-xl font-semibold">Currency Meter 1D</h1>
                        <StrengthTable columns={this.state.columns} data={this.state.change_1D} />
                    </div>
                </main>
            </div>
        );
    }
}

