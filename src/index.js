import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import CurrienciesProvider from './CurrienciesProvide';
import Sidebar from './Sidebar';
import { BrowserRouter as Router } from 'react-router-dom';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

ReactDOM.render(
  <Router>
    <div className="App">
      <NotificationContainer />
      <Sidebar />
    </div>
  </Router>
  , document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
